# Drones

## Technical Docuumentation

[[_TOC_]]

---

:scroll: **START**

### Introduction

This is a drone fleet management system - the new disruptive force in logistics

---

### Technical specification

This service is built with the following

- Spring Boot Framework v2.7
- H2 in-memory database
- Features a cron job that runs every other minute to poll drone statistics to /var/log/drones/app.log. This path can be customized by passing in environment variables in **docker-compose.yaml** file
- A database initialization script, **data.sql** hosted in the application resources directory

---

### Requirements

- [Ensure you have docker installed](https://docs.docker.com/engine/install/)
- 1G of system memory
- 1G of storage

---

### Steps to Build and run

- Download project directory
- Navigate to project root containing **docker-compose.yaml**
- Open a command prompt to the file location
- Ensure the log file exists and is accessible
  ``` mkdir logs && chmod -R +w logs ```
- Run the following command to start the application
  ``` docker-compose up ```

---

### Accessing the Service Endpoints

#### [Get Drones](GET <http://localhost:8000/drones>)

Payload:

Sample Response:

```shell
{
  "code": 200,
  "message": "OK",
  "data": [
    {
      "uuid": "9a573fcf-b916-4072-874e-165b2c2f88d9",
      "serialNumber": "1",
      "model": "LIGHTWEIGHT",
      "weightLimit": 120,
      "weight": 90,
      "batteryCapacity": 100.0,
      "state": "LOADING"
    },
    {
      "uuid": "08ab4165-fdae-47c3-92cc-d9ae7f1c5024",
      "serialNumber": "2",
      "model": "HEAVYWEIGHT",
      "weightLimit": 500,
      "weight": 0,
      "batteryCapacity": 100.0,
      "state": "IDLE"
    }
  ]
} 
```

#### [Get Drone](GET <http://localhost:8000/drones/{droneId}>)

Payload:
  pathVariable: droneId
  
Sample Response:

```shell
{
  "code": 200,
  "message": "OK",
  "data": {
    "uuid": "f4999de1-0eb7-46da-9fa7-58b9ca8e6ecc",
    "serialNumber": "006",
    "model": "MIDDLEWEIGHT",
    "weightLimit": 210,
    "weight": 210,
    "batteryCapacity": 85.0,
    "state": "LOADING"
  }
}
```

#### [Register Drone](POST <http://localhost:8000/drones>)

Payload:

```shell
{
  "serialNumber":"006",
  "model":"middleweight",
  "batteryCapacity": 85,
  "weightLimit":210
}
```

Sample Response:

```shell
{
  "code": 201,
  "message": "Drone has been successfully registered",
  "data": {
    "uuid": "f4999de1-0eb7-46da-9fa7-58b9ca8e6ecc",
    "serialNumber": "006",
    "model": "MIDDLEWEIGHT",
    "weightLimit": 210,
    "weight": 0,
    "batteryCapacity": 85.0,
    "state": "IDLE"
  }
}
```

#### [Prepare Drone for Loading](PATCH <http://localhost:8000/drones/{droneId}/prepare>)

Payload:

Sample Response:

```shell
{
  "code": 200,
  "message": "Drone is ready for loading"
}
```

#### [Get Battery Level](GET <http://localhost:8000/drones/{droneId}/battery-level>)

Payload:

Sample Response:

```shell
{
  "code": 200,
  "message": "OK",
  "data": {
    "droneId": "06364cbc-9468-4bfe-a917-1ab610bd49f1",
    "batteryLevel": 100
  }
}
```

#### [Load Medication onto a drone](POST <http://localhost:8000/drones/{droneId}/medications>)

Payload:

```shell
curl -X POST \
  'http://localhost:8080/api/v1/drones/9a573fcf-b916-4072-874e-165b2c2f88d9/medications' \
  --header 'Accept: */*' \
  --header 'User-Agent: Thunder Client (https://www.thunderclient.com)' \
  --form 'medication="{ "name":"Gold_Standard", "code":"GSL_01", "weight":1 }"' \
  --form 'file=@/home/folarin/Pictures/android-chrome-192x192.png'
```

Sample Response:

```shell
{
  "code": 201,
  "data": {
    "uuid": "5f207e59-7884-452d-aed3-1c4cb1577b72",
    "name": "Gold_Standard",
    "weight": 1,
    "code": "GSL_01",
    "droneId": "9a573fcf-b916-4072-874e-165b2c2f88d9",
    "image": "iVBORw0KGgoAAAANSUhEUgAAAM..."
  }
}
```

#### [Get Drone Medications](GET <http://localhost:8000/drones/{droneId}/medications>)

Payload:

Sample Response:

```shell
{
  "code": 200,
  "message": "OK",
  "data": [
    {
      "uuid": "3cc109d0-2833-4250-9cfb-6baee7e1146f",
      "name": "DRUG_1",
      "weight": 10,
      "code": "GSL_01",
      "droneId": "9a573fcf-b916-4072-874e-165b2c2f88d9",
      "image": "eC1pbWFnZS1kYXRh..."
    },
    {
      "uuid": "5f207e59-7884-452d-aed3-1c4cb1577b72",
      "name": "Gold_Standard",
      "weight": 1,
      "code": "GSL_01",
      "droneId": "9a573fcf-b916-4072-874e-165b2c2f88d9",
      "image": "iVBORw0KGgoAAA..."
    }
  ]
}
```

#### [Get Available Drones in Loading State with Capacity](GET <http://localhost:8000/drones/{droneId}/medications>)

Payload:

Sample Response:

```shell
{
  "code": 200,
  "message": "OK",
  "data": [
    {
      "uuid": "9a573fcf-b916-4072-874e-165b2c2f88d9",
      "serialNumber": "1",
      "model": "LIGHTWEIGHT",
      "weightLimit": 120,
      "weight": 91,
      "batteryCapacity": 100.0,
      "state": "LOADING"
    }
  ]
}
```

---

### TODO

- Unit tests
- Create an OpenAPI 3.0 spec for this service
---

:scroll: **END**
