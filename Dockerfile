ARG JAVA_VERSION=17
ARG JDK_PLATFORM=linux/amd64
FROM --platform=${JDK_PLATFORM} openjdk:${JAVA_VERSION}-alpine AS openjdk
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /app/app.jar
ENTRYPOINT ["java", "-jar", "/app/app.jar"]