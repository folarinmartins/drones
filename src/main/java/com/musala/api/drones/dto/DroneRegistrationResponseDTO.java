package com.musala.api.drones.dto;

import org.springframework.stereotype.Component;

import com.musala.api.drones.model.Drone;

import java.util.UUID;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
@Component
public class DroneRegistrationResponseDTO {
	private UUID uuid;
	private String serialNumber;
	private Drone.Model model;
	private int weightLimit;
	private int weight;
	private float batteryCapacity;
	private Drone.State state;
}
