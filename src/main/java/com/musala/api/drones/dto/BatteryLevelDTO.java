package com.musala.api.drones.dto;

import java.util.UUID;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class BatteryLevelDTO {
	private UUID droneId;
	private int batteryLevel;
}
