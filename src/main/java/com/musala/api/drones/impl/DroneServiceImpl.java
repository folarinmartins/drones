package com.musala.api.drones.impl;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.musala.api.drones.dto.BatteryLevelDTO;
import com.musala.api.drones.dto.DroneRegistrationDTO;
import com.musala.api.drones.dto.DroneRegistrationResponseDTO;
import com.musala.api.drones.dto.MedicationResponseDTO;
import com.musala.api.drones.dto.ResponseDTO;
import com.musala.api.drones.model.Drone;
import com.musala.api.drones.repository.DroneRepository;
import com.musala.api.drones.repository.MedicationRepository;
import com.musala.api.drones.service.DroneService;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Slf4j
@Service
@EnableScheduling
public class DroneServiceImpl implements DroneService {

	@Autowired
	DroneRepository droneRepository;

	@Autowired
	MedicationRepository medicationRepository;

	@Autowired
	MedicationServiceImpl medicationServiceImpl;

	@Override
	public ResponseDTO<List<DroneRegistrationResponseDTO>> getAvailableDronesForLoading(Pageable page) {
		return Mono.just(page)
				.subscribeOn(Schedulers.parallel())
				.map(pageable -> {
					var response = new ResponseDTO<List<DroneRegistrationResponseDTO>>();
					response.setCode(HttpStatus.OK.value());
					response.setMessage(HttpStatus.OK.getReasonPhrase());
					response.setData(generateDroneResponseDto(droneRepository.getAllDronesAvailableForLoading(page)));
					return response;
				}).block();
	}

	@Override
	public ResponseDTO<DroneRegistrationResponseDTO> getDrone(UUID droneUUID) {
		var drone = droneRepository.findByUuid(droneUUID);
		var droneResponseDto = new DroneRegistrationResponseDTO();
		var response = new ResponseDTO<DroneRegistrationResponseDTO>();

		if (drone == null) {
			throw new RuntimeException("Drone with provided Id has not been registered");
		}

		BeanUtils.copyProperties(drone, droneResponseDto, "id");
		droneResponseDto.setUuid(droneUUID);
		droneResponseDto.setWeight(drone.getWeight());
		droneResponseDto.setBatteryCapacity(drone.getBatteryCapacity());
		response.setData(droneResponseDto);
		response.setCode(HttpStatus.OK.value());
		response.setMessage(HttpStatus.OK.getReasonPhrase());

		return response;
	}

	@Override
	public ResponseDTO<List<DroneRegistrationResponseDTO>> getDroneList(Pageable page) {
		return Mono.just(page)
				.subscribeOn(Schedulers.parallel())
				.map(pageable -> {
					var response = new ResponseDTO<List<DroneRegistrationResponseDTO>>();
					response.setCode(HttpStatus.OK.value());
					response.setMessage(HttpStatus.OK.getReasonPhrase());
					response.setData(generateDroneResponseDto(droneRepository.findAll()));
					return response;
				}).block();
	}

	@Override
	public ResponseDTO<DroneRegistrationResponseDTO> prepareDroneForLoading(UUID droneUUID) {
		var drone = droneRepository.findByUuid(droneUUID);
		var response = new ResponseDTO<DroneRegistrationResponseDTO>();
		if (drone.getBatteryCapacity() < 25) {
			throw new RuntimeException("Battery is too low for drone to be loaded");
		}

		if (drone.getState().equals(Drone.State.LOADING)) {
			throw new RuntimeException("Drone is already in LOADING state");
		}

		if (drone.getWeight() == drone.getWeightLimit()) {
			throw new RuntimeException("Drone cannot be loaded as the maximum drone weight has been reached");
		}
		drone.setState(Drone.State.LOADING);
		droneRepository.save(drone);

		response.setCode(HttpStatus.OK.value());
		response.setMessage("Drone is ready for loading");

		return response;
	}

	@Override
	public ResponseDTO<DroneRegistrationResponseDTO> registerDrone(DroneRegistrationDTO drone) {
		var newDrone = new Drone();
		var response = new ResponseDTO<DroneRegistrationResponseDTO>();
		var registeredDrone = new DroneRegistrationResponseDTO();

		if (drone.getBatteryCapacity() > 100 || drone.getBatteryCapacity() < 1) {
			throw new RuntimeException("Battery capacity must be between 1 and 100, inclusive");
		}

		if (drone.getWeightLimit() > 500) {
			throw new RuntimeException("Weight Limit cannot exceed 500gr");
		}

		try {
			Drone.Model droneModel = Drone.Model.valueOf(drone.getModel().toUpperCase());
			newDrone.setModel(droneModel);
		} catch (Exception e) {
			throw new RuntimeException("Invalid Drone Model specified");
		}

		newDrone.setUuid(UUID.randomUUID());

		BeanUtils.copyProperties(drone, newDrone);

		newDrone.setState(Drone.State.IDLE);
		newDrone = droneRepository.save(newDrone);

		BeanUtils.copyProperties(newDrone, registeredDrone, "id", "medications");
		registeredDrone.setUuid(newDrone.getUuid());
		registeredDrone.setBatteryCapacity(newDrone.getBatteryCapacity());

		response.setData(registeredDrone);
		response.setCode(HttpStatus.CREATED.value());
		response.setMessage("Drone has been successfully registered");

		return response;
	}

	@Override
	public ResponseDTO<BatteryLevelDTO> getDroneBatteryLevel(UUID droneUid) {
		var drone = droneRepository.findByUuid(droneUid);
		var response = new ResponseDTO<BatteryLevelDTO>();
		var battLevelDto = new BatteryLevelDTO();

		if (drone == null) {
			throw new RuntimeException("Drone with provided Id has not been registered");
		}

		battLevelDto.setBatteryLevel(drone.getBatteryCapacity());
		battLevelDto.setDroneId(drone.getUuid());

		response.setData(battLevelDto);
		response.setCode(HttpStatus.OK.value());
		response.setMessage(HttpStatus.OK.getReasonPhrase());

		return response;
	}

	@Override
	public ResponseDTO<List<MedicationResponseDTO>> getAllDroneMedications(UUID droneId, Pageable pageable) {
		var drone = droneRepository.findByUuid(droneId);
		return Mono.just(pageable)
				.subscribeOn(Schedulers.parallel())
				.map(page -> {
					var responseDTO = new ResponseDTO<List<MedicationResponseDTO>>();
					responseDTO.setCode(HttpStatus.OK.value());
					responseDTO.setMessage(HttpStatus.OK.getReasonPhrase());
					responseDTO.setData(medicationServiceImpl.generateMedicationResponseDto(
							medicationRepository.getAllMedicationsForDrone(drone.getId(), pageable)));
					return responseDTO;
				}).block();
	}

	@Override
	@Scheduled(fixedRate = 2, timeUnit = TimeUnit.MINUTES)
	public void checkDronesBatteryLevels() {
		List<BatteryLevelDTO> battLevels = droneRepository.findAll().stream()
				.map(drone -> {
					var batteryLevel = new BatteryLevelDTO();
					batteryLevel.setBatteryLevel(drone.getBatteryCapacity());
					batteryLevel.setDroneId(drone.getUuid());
					return batteryLevel;
				}).collect(Collectors.toList());
		log.info(battLevels.toString());
	}

	private List<DroneRegistrationResponseDTO> generateDroneResponseDto(List<Drone> droneList) {
		return droneList.stream()
				.map(drone -> {
					var droneResponseDto = new DroneRegistrationResponseDTO();
					BeanUtils.copyProperties(drone, droneResponseDto, "id");
					droneResponseDto.setUuid(drone.getUuid());
					droneResponseDto.setBatteryCapacity(drone.getBatteryCapacity());
					droneResponseDto.setWeight(drone.getWeight());
					return droneResponseDto;
				}).collect(Collectors.toList());
	}
}
