package com.musala.api.drones.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.api.drones.dto.MedicationDTO;
import com.musala.api.drones.dto.MedicationResponseDTO;
import com.musala.api.drones.dto.ResponseDTO;
import com.musala.api.drones.model.Drone;
import com.musala.api.drones.model.Medication;
import com.musala.api.drones.service.MedicationService;
import com.musala.api.drones.repository.MedicationRepository;
import com.musala.api.drones.repository.DroneRepository;

@Service
public class MedicationServiceImpl implements MedicationService {
	@Autowired
	MedicationRepository medicationRepo;

	@Autowired
	DroneRepository droneRepo;

	@Override
	public ResponseDTO<MedicationResponseDTO> addMedication(MedicationDTO medicationDto, byte[] image) {
		Medication medication = new Medication();
		var response = new ResponseDTO<MedicationResponseDTO>();
		var medicationResponseDto = new MedicationResponseDTO();
		var drone = droneRepo.findByUuid(medicationDto.getDroneId());

		if (!medicationDto.getName().matches("^[a-zA-Z0-9_-]*$")) {
			throw new RuntimeException("Only letters, numbers, underscore and dash are allowed for name");
		}

		if (!medicationDto.getCode().matches("^[A-Z0-9_]*$")) {
			throw new RuntimeException("Only upper case letters, underscore and numbers are allowed for code");
		}

		if (drone == null) {
			throw new RuntimeException("Drone does not exist");
		}

		if (!drone.getState().equals(Drone.State.LOADING)) {
			throw new RuntimeException("The specified drone is not available for loading");
		}

		if (medicationDto.getWeight() + drone.getWeight() > drone.getWeightLimit()) {
			throw new RuntimeException("Maximum weight capacity for the drone has been reached");
		}

		BeanUtils.copyProperties(medicationDto, medication, "drone");
		medication.setDrone(drone);
		medication.setImage(image);
		medication.setUuid(UUID.randomUUID());
		medication = medicationRepo.save(medication);

		drone.setWeight(drone.getWeight() + medicationDto.getWeight());
		// drone.getMedications().add(medication);
		drone = droneRepo.save(drone);

		BeanUtils.copyProperties(medication, medicationResponseDto, "id", "drone");
		medicationResponseDto.setUuid(medication.getUuid());
		medicationResponseDto.setDroneId(medication.getDrone().getUuid());
		medicationResponseDto.setImage(medication.getImage());
		response.setData(medicationResponseDto);
		response.setCode(HttpStatus.CREATED.value());

		return response;
	}

	@Override
	public MedicationDTO parseJson(String medication) {
		var medicationDto = new MedicationDTO();
		try {
			var objectMapper = new ObjectMapper();
			medicationDto = objectMapper.readValue(medication, MedicationDTO.class);
		} catch (JsonProcessingException e) {
			throw new RuntimeException("An error occurred while parsing json string");
		}
		return medicationDto;
	}

	List<MedicationResponseDTO> generateMedicationResponseDto(List<Medication> medicationList) {
		return medicationList.stream()
				.map(medication -> {
					var medicationResponseDto = new MedicationResponseDTO();
					BeanUtils.copyProperties(medication, medicationResponseDto, "id");
					medicationResponseDto.setUuid(medication.getUuid());
					medicationResponseDto.setWeight(medication.getWeight());
					medicationResponseDto.setDroneId(medication.getDrone().getUuid());
					medicationResponseDto.setImage(medication.getImage());
					return medicationResponseDto;
				}).collect(Collectors.toList());
	}

}
