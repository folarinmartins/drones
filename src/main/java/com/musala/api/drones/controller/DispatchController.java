package com.musala.api.drones.controller;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpStatus;

import com.musala.api.drones.dto.DroneRegistrationDTO;
import com.musala.api.drones.dto.DroneRegistrationResponseDTO;
import com.musala.api.drones.dto.MedicationDTO;
import com.musala.api.drones.dto.MedicationResponseDTO;
import com.musala.api.drones.dto.ResponseDTO;
import com.musala.api.drones.impl.DroneServiceImpl;
import com.musala.api.drones.impl.MedicationServiceImpl;

@RestController
@RequestMapping(path = { "/api/v1/drones" })
public class DispatchController {
	@Autowired
	DroneServiceImpl droneService;

	@Autowired
	MedicationServiceImpl medicationService;

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseDTO<DroneRegistrationResponseDTO>> registerDrone(
			@Valid @RequestBody DroneRegistrationDTO drone) {
		return ResponseEntity.ok(droneService.registerDrone(drone));
	}

	@GetMapping()
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<ResponseDTO<List<DroneRegistrationResponseDTO>>> getAllDrones(
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "100") int size) {
		Pageable pageable = PageRequest.of(page, size, Sort.by("id"));
		return ResponseEntity.ok().body(droneService.getDroneList(pageable));
	}

	@GetMapping("/{droneUid}")
	public ResponseEntity<?> getDrone(@PathVariable("droneUid") UUID droneUid) {
		return ResponseEntity.ok(droneService.getDrone(droneUid));
	}

	@PatchMapping("/{droneUid}/prepare")
	public ResponseEntity<?> prepareDroneForLoading(@PathVariable("droneUid") UUID droneUid) {
		return ResponseEntity.ok(droneService.prepareDroneForLoading(droneUid));
	}

	@GetMapping("/available")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<ResponseDTO<List<DroneRegistrationResponseDTO>>> getAllAvailableDrones(
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "100") int size) {
		Pageable pageable = PageRequest.of(page, size, Sort.by("id"));
		return ResponseEntity.ok().body(droneService.getAvailableDronesForLoading(pageable));
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(path = "/{droneUid}/medications", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> addMedication(@PathVariable("droneUid") UUID droneUid,
			@RequestPart("file") MultipartFile file,
			@RequestPart("medication") String medication) throws IOException {
		if (file == null)
			throw new RuntimeException("medication image found");
		byte[] bytes = file.getBytes();
		MedicationDTO medicationDto = medicationService.parseJson(medication);
		medicationDto.setDroneId(droneUid);
		return ResponseEntity.ok().body(medicationService.addMedication(medicationDto, bytes));
	}

	@GetMapping("/{droneUid}/medications")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<ResponseDTO<List<MedicationResponseDTO>>> getAllDroneMedications(
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "100") int size, @PathVariable("droneUid") UUID droneUid) {
		Pageable pageable = PageRequest.of(page, size, Sort.by("id"));
		return ResponseEntity.ok().body(droneService.getAllDroneMedications(droneUid, pageable));
	}

	@GetMapping("/{droneUid}/battery-level")
	public ResponseEntity<?> getDroneBatteryLevel(@PathVariable("droneUid") UUID droneUid) {
		return ResponseEntity.ok(droneService.getDroneBatteryLevel(droneUid));
	}
}
