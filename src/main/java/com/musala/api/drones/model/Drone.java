package com.musala.api.drones.model;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "drones")
public class Drone implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "uid", length = 32, columnDefinition = "uuid NOT NULL", unique = true, nullable = false)
	private UUID uuid;

	@Column(name = "serial_no", length = 100, nullable = false)
	private String serialNumber;

	@Enumerated(EnumType.STRING)
	@Column(name = "model", nullable = false)
	private Model model;

	@Column(name = "weight_limit_gr", nullable = false)
	private int weightLimit;

	@Column(name = "battery_capacity", nullable = false)
	private int batteryCapacity;

	@Enumerated(EnumType.STRING)
	@Column(name = "state", nullable = false)
	private State state;

	@Column(name = "payload_weight", columnDefinition = "integer default 0", nullable = false)
	private int weight;

	@OneToMany(mappedBy = "drone", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private List<Medication> medications;

	public void addMedication(Medication medication) {
		medications.add(medication);
		medication.setDrone(this);
	}

	public void removeMedication(Medication medication) {
		medications.remove(medication);
		medication.setDrone(null);
	}

	public enum Model {
		LIGHTWEIGHT, MIDDLEWEIGHT, CRUISERWEIGHT, HEAVYWEIGHT;
	}

	public enum State {
		IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
	}
}