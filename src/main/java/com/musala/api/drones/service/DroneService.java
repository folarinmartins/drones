package com.musala.api.drones.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;

import com.musala.api.drones.dto.BatteryLevelDTO;
import com.musala.api.drones.dto.DroneRegistrationDTO;
import com.musala.api.drones.dto.DroneRegistrationResponseDTO;
import com.musala.api.drones.dto.MedicationResponseDTO;
import com.musala.api.drones.dto.ResponseDTO;

public interface DroneService {
	ResponseDTO<DroneRegistrationResponseDTO> registerDrone(DroneRegistrationDTO droneRegistrationDTO);

	ResponseDTO<List<DroneRegistrationResponseDTO>> getDroneList(Pageable page);

	ResponseDTO<DroneRegistrationResponseDTO> getDrone(UUID droneUUID);

	ResponseDTO<DroneRegistrationResponseDTO> prepareDroneForLoading(UUID droneUUID);

	ResponseDTO<List<DroneRegistrationResponseDTO>> getAvailableDronesForLoading(Pageable page);

	ResponseDTO<List<MedicationResponseDTO>> getAllDroneMedications(UUID droneID, Pageable pageable);

	public ResponseDTO<BatteryLevelDTO> getDroneBatteryLevel(UUID droneUid);

	void checkDronesBatteryLevels();
}
