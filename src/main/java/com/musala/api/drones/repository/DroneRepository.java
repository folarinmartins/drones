package com.musala.api.drones.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.musala.api.drones.model.Drone;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {
	Drone findByUuid(UUID droneUUID);

	@Query(value = "SELECT * FROM drones  WHERE state='LOADING' AND payload_weight<weight_limit_gr", nativeQuery = true)
	List<Drone> getAllDronesAvailableForLoading(Pageable pageable);
}
