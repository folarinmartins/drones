package com.musala.api.drones.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.musala.api.drones.model.Medication;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long> {
	Medication findByUuid(UUID uuid);

	@Query(value = "SELECT * FROM medications  WHERE drone=:droneId", nativeQuery = true)
	List<Medication> getAllMedicationsForDrone(@Param("droneId") Long droneId, Pageable pageable);

}
